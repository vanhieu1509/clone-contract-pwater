<?php

function get_columns_from_table($table, $remove_id = true)
{
    $columns = "DESCRIBE $table";
    $result = mysql_query($columns);
    $col = array();
    if ($result) {
        while ($row = mysql_fetch_assoc($result)) {
            $col[] = $row['Field'];
        }
    }
    if ($remove_id) {
        unset($col[0]);
    }
    return $col;
}

function get_data_from_table($table, $condition = array(), $remove_id = true)
{
    $query = "SELECT * FROM $table";
    if ($condition) {
        $numItems = count($condition);
        $i = 0;
        $query .= " WHERE ";
        foreach ($condition as $key => $value) {
            $query .= $key . ' = ' . $value;
            if (++$i !== $numItems) {
                $query .= ' AND ';
            }
        }
    }
    $result = mysql_query($query);
    $rs_arr = array();
    if ($result) {
        while ($row = mysql_fetch_assoc($result)) {
            if ($remove_id == true) {
                unset($row['id']);
            }
            if (isset($row['contract_id']) && $row['contract_id'] != 0) {
                $row['contract_id'] = "@contract_id";
            }
            if (isset($row['customer_id']) && $row['customer_id'] != 0) {
                $row['customer_id'] = "@contract_id";
            }
            if (isset($row['contract_acquisition_id'])) {
                $row['contract_acquisition_id'] = "@contract_acquisition_id";
            }
            if (isset($row['contract_amazon_id'])) {
                $row['contract_amazon_id'] = "@contract_amazon_id";
            }
            if (isset($row['contract_bill_id'])) {
                $row['contract_bill_id'] = "@contract_bill_id";
            }
            if (isset($row['contract_delivery_id'])) {
                $row['contract_delivery_id'] = "@contract_delivery_id";
            }
            $rs_arr[] = $row;
        }
    }
    return $rs_arr;
}

function export_mysql_string($table, $condition = array())
{
    $array_col = get_columns_from_table($table);
    $data = get_data_from_table($table, $condition);
    if (empty($data)) {
        echo 'NOT FOUND DATA';
        exit;
    }
    $query = "INSERT INTO $table (" . implode(',', $array_col) . ")  VALUES ";
    $i = 0;
    $numItems = count($data);
    foreach ($data as $value) {
        $query .= '("' . implode('", "', $value) . '")';
        if (++$i !== $numItems) {
            $query .= ' , ';
        }
    }
    return $query;
}

function export_mysql_string_2($table, $array_col, $data)
{
    $query = '';
    if (!empty($data)) {
        $query = "INSERT INTO $table (" . implode(',', $array_col) . ")  VALUES ";
        $i = 0;
        $numItems = count($data);
        foreach ($data as $value) {
            $query .= '("' . implode('", "', $value) . '")';
            if (++$i !== $numItems) {
                $query .= ' , ';
            }
        }
    }
    return $query;
}

function export_mysql_string_3($table, $array_col, $data)
{
    $query = '';
    if (!empty($data)) {
        $query = "INSERT INTO $table (" . implode(',', $array_col) . ")  VALUES ";
        $query .= '("' . implode('", "', $data) . '")';

    }
    return $query;
}

function replace_query($query)
{
    $query = str_replace('"@contract_id"', '@contract_id', $query);
    $query = str_replace('"@contract_bill_id"', '@contract_bill_id', $query);

    $query = str_replace('"@contract_acquisition_id"', '@contract_acquisition_id', $query);

    $query = str_replace('"@contract_amazon_id"', '@contract_amazon_id', $query);
    $query = str_replace('"@contract_delivery_id"', '@contract_delivery_id', $query);

    return $query;

}

function clone_table_history($table, $contract_id, $is_contract_id = true)
{
    global $fp;
    $data = array();
    $array_col = array();
    if ($is_contract_id) {
        $data = get_data_from_table($table, array('contract_id' => $contract_id), false);
    } else {
        $data = get_data_from_table($table, array('customer_id' => $contract_id), false);
    }
    $array_col = get_columns_from_table($table);
    $index = 0;
    foreach ($data as $value) {
        $sql4 = '';
        $sql = '';
        $data_child = array();
        $array_col_history = array();
        $data_child = get_data_from_table_history($table, array($table . "_id" => $value['id']), $index);
        $array_col_history = get_columns_from_table($table . "_history");
        unset($value['id']);
        $sql = export_mysql_string_3($table, $array_col, $value);
        $sql = replace_query($sql);
        if ($sql != '') {
            fwrite($fp, $sql . ";\r\n");
            fwrite($fp, "SET @" . "$table" . "_id" . $index . " = LAST_INSERT_ID();" . "\r\n");
            $sql4 = export_mysql_string_2($table . "_history", $array_col_history, $data_child);
            $sql4 = str_replace('"@' . "$table" . "_id" . $index . '"', '@' . "$table" . "_id" . $index, $sql4);
            if ($sql4 != '') {
                fwrite($fp, $sql4 . ";\r\n");
            }
            $index++;
        }
    }
}

function clone_table_manual($table, $contract_id)
{
    global $fp;
    $data = array();
    $array_col = array();
    $sql = '';
    $data = get_data_from_table($table, array('contract_id' => $contract_id));
    $array_col = get_columns_from_table($table);
    $sql = export_mysql_string_2($table, $array_col, $data);
    $sql = replace_query($sql);
    if ($sql != '') {
        fwrite($fp, $sql . ";\r\n");
    }
}

function get_data_from_table_history($table, $condition = array(), $index = '')
{
    $table_history = $table . "_history";
    $query = "SELECT * FROM $table_history";
    if ($condition) {
        $numItems = count($condition);
        $i = 0;
        $query .= " WHERE ";
        foreach ($condition as $key => $value) {
            $query .= $key . ' = ' . $value;
            if (++$i !== $numItems) {
                $query .= ' AND ';
            }
        }
    }
    $result = mysql_query($query);
    $rs_arr = array();
    if ($result) {
        while ($row = mysql_fetch_assoc($result)) {
            unset($row['id']);

            if (isset($row["$table" . "_id"])) {
                $row["$table" . "_id"] = "@" . "$table" . "_id" . $index;
            }
            $rs_arr[] = $row;
        }
    }

    return $rs_arr;
}

function process_export_order_data($contract_id)
{
    global $fp;
    $contract_orders = get_data_from_table('contract_order', array('contract_id' => $contract_id), false);
    $column_order_detail = get_columns_from_table('order_detail');
    $column_contract_order = get_columns_from_table('contract_order');
    $column_order_delivery = get_columns_from_table('order_delivery');
    $column_order_acquisition = get_columns_from_table('order_acquisition');
    $column_order_product = get_columns_from_table('order_product');
    $column_order_settlement = get_columns_from_table('order_settlement');
    $column_order_settlement_detail = get_columns_from_table('order_settlement_detail');
    $column_apply_cancel_settlement = get_columns_from_table('apply_cancel_settlement');
    $column_apply_claim_fee = get_columns_from_table('apply_claim_fee');
    $column_contract_direct_debit_mizuho = get_columns_from_table('contract_direct_debit_mizuho');
    $column_settlement_direct_debit_mizuho_log = get_columns_from_table('settlement_direct_debit_mizuho_log');
    $column_settlement_direct_debit_mizuho_detail_log = get_columns_from_table('settlement_direct_debit_mizuho_detail_log');
    $mapping_order_product = array();
    $mapping_order_settlement = array();
    $mapping_order_detail = array();
    $mapping_contract_direct_debit_mizuho = array();
    $mapping_settlement_direct_debit_mizuho_log = array();
    $i = 0;
	fwrite($fp, "SET @order_detail_id = 0;" . "\r\n");
    foreach ($contract_orders as $contract_order) {
        $j = 0;
        $order_details = get_data_from_table_2('order_detail', array('contract_order_id' => $contract_order['id']), false, $i);
        unset($contract_order['id']);
        $sql = export_mysql_string_3('contract_order', $column_contract_order, $contract_order);
        $sql = replace_query($sql);
        if ($sql != '') {
            fwrite($fp, $sql . ";\r\n");
            fwrite($fp, "SET @contract_order_id" . $i . " = LAST_INSERT_ID();" . "\r\n");
        }
        foreach ($order_details as $order_detail) {
            $x = 0;
            $order_detail_id = $order_detail['id'];
            if (!isset($mapping_order_detail[$order_detail_id])) {
                $mapping_order_detail[$order_detail_id] = $i . $j;
            }
            $data_order_delivery = get_data_from_table_2('order_delivery', array('order_detail_id' => $order_detail['id']), true, $i . $j);
            $data_order_acquisition = get_data_from_table_2('order_acquisition', array('order_detail_id' => $order_detail['id']), true, $i . $j);
            $data_order_settlements = get_data_from_table_2('order_settlement', array('order_detail_id' => $order_detail['id']), false, $i . $j);
            $data_order_products = get_data_from_table_2('order_product', array('order_detail_id' => $order_detail['id']), false, $i . $j);
            unset($order_detail['id']);
            $sql = export_mysql_string_3('order_detail', $column_order_detail, $order_detail);
            $sql = str_replace('"@contract_order_id' . $i . '"', '@contract_order_id' . $i, $sql);
            fwrite($fp, $sql . ";\r\n");
            fwrite($fp, "SET @order_detail_id" . $i . $j . " = LAST_INSERT_ID();" . "\r\n");
            $sql = export_mysql_string_2('order_delivery', $column_order_delivery, $data_order_delivery);
            $sql = str_replace('"@order_detail_id' . $i . $j . '"', '@order_detail_id' . $i . $j, $sql);
            if ($sql != '') {
                fwrite($fp, $sql . ";\r\n");
            }
            $sql = export_mysql_string_2('order_acquisition', $column_order_acquisition, $data_order_acquisition);
            $sql = str_replace('"@order_detail_id' . $i . $j . '"', '@order_detail_id' . $i . $j, $sql);
            if ($sql != '') {
                fwrite($fp, $sql . ";\r\n");
            }

            $data_apply_claim_fees = get_data_from_table_2('apply_claim_fee', array('order_detail_id' => $order_detail_id), true, $i . $j);
            $sql = export_mysql_string_2('apply_claim_fee', $column_apply_claim_fee, $data_apply_claim_fees);
            $sql = str_replace('"@order_detail_id' . $i . $j . '"', '@order_detail_id' . $i . $j, $sql);
            $sql = replace_query($sql);
            if ($sql != '') {
                fwrite($fp, $sql . ";\r\n");
            }
            foreach ($data_order_products as $data_order_product) {
                $y = 0;
                $order_product_id = $data_order_product['id'];
                if (!isset($mapping_order_product[$order_product_id])) {
                    $mapping_order_product[$order_product_id] = $i . $j . $x;
                    unset($data_order_product['id']);
                    $sql = export_mysql_string_3('order_product', $column_order_product, $data_order_product);
                    $sql = str_replace('"@order_detail_id' . $i . $j . '"', '@order_detail_id' . $i . $j, $sql);
                    fwrite($fp, $sql . ";\r\n");
                    fwrite($fp, "SET @order_product_id" . $i . $j . $x . " = LAST_INSERT_ID();" . "\r\n");
                }
                foreach ($data_order_settlements as $data_order_settlement) {
                    $order_settlement_id = $data_order_settlement['id'];
                    $z = 0;
                    if (!isset($mapping_order_settlement[$order_settlement_id])) {
                        $mapping_order_settlement[$order_settlement_id] = $i . $j . $x . $y;
                        unset($data_order_settlement['id']);
                        $sql = export_mysql_string_3('order_settlement', $column_order_settlement, $data_order_settlement);
                        $sql = str_replace('"@order_detail_id' . $i . $j . '"', '@order_detail_id' . $i . $j, $sql);
                        fwrite($fp, $sql . ";\r\n");
                        fwrite($fp, "SET @order_settlement_id" . $i . $j . $x . $y . " = LAST_INSERT_ID();" . "\r\n");
                        $data_apply_cancel_settlements = get_data_from_table_2('apply_cancel_settlement', array('order_settlement_id' => $order_settlement_id), true, $mapping_order_settlement[$order_settlement_id]);
                        $sql = export_mysql_string_2('apply_cancel_settlement', $column_apply_cancel_settlement, $data_apply_cancel_settlements);
                        $sql = str_replace('"@order_settlement_id' . $mapping_order_settlement[$order_settlement_id] . '"', '@order_settlement_id' . $mapping_order_settlement[$order_settlement_id], $sql);
                        if ($sql != '') {
                            fwrite($fp, $sql . ";\r\n");
                        }
                    }
                    $data_order_settement_details = get_data_from_table_2('order_settlement_detail', array('order_settlement_id' => $order_settlement_id, 'order_product_id' => $order_product_id));
                    foreach ($data_order_settement_details as $data_order_settement_detail) {
                        $data_order_settement_detail['order_settlement_id'] = "@order_settlement_id" . $mapping_order_settlement[$order_settlement_id];
                        $data_order_settement_detail['order_product_id'] = "@order_product_id" . $mapping_order_product[$data_order_settement_detail['order_product_id']];
                        $sql = export_mysql_string_3('order_settlement_detail', $column_order_settlement_detail, $data_order_settement_detail);
                        $sql = str_replace('"@order_settlement_id' . $mapping_order_settlement[$order_settlement_id] . '"', '@order_settlement_id' . $mapping_order_settlement[$order_settlement_id], $sql);
                        $sql = str_replace('"@order_product_id' . $mapping_order_product[$order_product_id] . '"', '@order_product_id' . $mapping_order_product[$order_product_id], $sql);
                        fwrite($fp, $sql . ";\r\n");
                        $z++;
                    }
                    $y++;
                }
                $x++;
            }
            $j++;
        }
        $i++;
    }

    // PROCESS EXPORT MIZUHO
    $data_contract_direct_debit_mizuhos = get_data_from_table_2('contract_direct_debit_mizuho', array('contract_id' => $contract_id), false);
    $index_mizuho_id = 0;
    $index_settlement_mizuho_log_id = 0;
    foreach ($data_contract_direct_debit_mizuhos as $data_contract_direct_debit_mizuho) {
        $mizuho_id = $data_contract_direct_debit_mizuho['id'];
        $mapping_contract_direct_debit_mizuho[$mizuho_id] = $index_mizuho_id;
        unset($data_contract_direct_debit_mizuho['id']);
        $sql = export_mysql_string_3('contract_direct_debit_mizuho', $column_contract_direct_debit_mizuho, $data_contract_direct_debit_mizuho);
        $sql = replace_query($sql);
        fwrite($fp, $sql . ";\r\n");
        fwrite($fp, "SET @contract_direct_debit_mizuho_id" . $mapping_contract_direct_debit_mizuho[$mizuho_id] . " = LAST_INSERT_ID();" . "\r\n");
        $index_mizuho_id++;
    }
    $data_settlement_direct_debit_mizuho_logs = get_data_from_table_2('settlement_direct_debit_mizuho_log', array('contract_id' => $contract_id), false);
    foreach ($data_settlement_direct_debit_mizuho_logs as $data_settlement_direct_debit_mizuho_log) {
        $settlement_mizuho_log_id = $data_settlement_direct_debit_mizuho_log['id'];
        $contract_direct_debit_mizuho_id = $data_settlement_direct_debit_mizuho_log['contract_direct_debit_mizuho_id'];
        $mapping_settlement_direct_debit_mizuho_log[$settlement_mizuho_log_id] = $index_settlement_mizuho_log_id;
        $data_settlement_direct_debit_mizuho_log['contract_direct_debit_mizuho_id'] = "@contract_direct_debit_mizuho_id" . $mapping_contract_direct_debit_mizuho[$contract_direct_debit_mizuho_id];
        unset($data_settlement_direct_debit_mizuho_log['id']);
        $sql = export_mysql_string_3('settlement_direct_debit_mizuho_log', $column_settlement_direct_debit_mizuho_log, $data_settlement_direct_debit_mizuho_log);
        $sql = replace_query($sql);
        $sql = str_replace('"@contract_direct_debit_mizuho_id' . $mapping_contract_direct_debit_mizuho[$contract_direct_debit_mizuho_id] . '"', '@contract_direct_debit_mizuho_id' . $mapping_contract_direct_debit_mizuho[$contract_direct_debit_mizuho_id], $sql);
        if ($sql != '') {
            fwrite($fp, $sql . ";\r\n");
            fwrite($fp, "SET @settlement_direct_debit_mizuho_log_id" . $mapping_settlement_direct_debit_mizuho_log[$settlement_mizuho_log_id] . " = LAST_INSERT_ID();" . "\r\n");
        }
        $index_settlement_mizuho_log_id++;
    }
    $list_settlement_mizuho_log_id = array_keys($mapping_settlement_direct_debit_mizuho_log);
    $data_settlement_direct_debit_mizuho_log_details = get_data_from_table_2('settlement_direct_debit_mizuho_detail_log', array('settlement_direct_debit_mizuho_log_id' => $list_settlement_mizuho_log_id), true);
    foreach ($data_settlement_direct_debit_mizuho_log_details as $data_settlement_direct_debit_mizuho_log_detail) {
        $order_detail_id = $data_settlement_direct_debit_mizuho_log_detail['order_detail_id'];
        $settlement_direct_debit_mizuho_log_id = $data_settlement_direct_debit_mizuho_log_detail['settlement_direct_debit_mizuho_log_id'];
        $data_settlement_direct_debit_mizuho_log_detail['settlement_direct_debit_mizuho_log_id'] = "@settlement_direct_debit_mizuho_log_id" . $mapping_settlement_direct_debit_mizuho_log[$data_settlement_direct_debit_mizuho_log_detail['settlement_direct_debit_mizuho_log_id']];
        $data_settlement_direct_debit_mizuho_log_detail['order_detail_id'] = "@order_detail_id" . $mapping_order_detail[$data_settlement_direct_debit_mizuho_log_detail['order_detail_id']];
        $sql = export_mysql_string_3('settlement_direct_debit_mizuho_detail_log', $column_settlement_direct_debit_mizuho_detail_log, $data_settlement_direct_debit_mizuho_log_detail);
        $sql = str_replace('"@settlement_direct_debit_mizuho_log_id' . $mapping_settlement_direct_debit_mizuho_log[$settlement_direct_debit_mizuho_log_id] . '"', '@settlement_direct_debit_mizuho_log_id' . $mapping_settlement_direct_debit_mizuho_log[$settlement_direct_debit_mizuho_log_id], $sql);
        $sql = str_replace('"@order_detail_id' . $mapping_order_detail[$order_detail_id] . '"', '@order_detail_id' . $mapping_order_detail[$order_detail_id], $sql);
        if ($sql != '') {
            fwrite($fp, $sql . ";\r\n");
        }
    }

    // PROCESS EXPORT COUPON
    $column_coupon_contract = get_columns_from_table('coupon_contract', false);
    $data_coupon_contracts = get_data_from_table_2('coupon_contract', array('id' => $contract_id), false);
    foreach ($data_coupon_contracts as &$data_coupon_contract) {
        $data_coupon_contract['id'] = '@contract_id';
    }
    $sql = export_mysql_string_2('coupon_contract', $column_coupon_contract, $data_coupon_contracts);
    $sql = replace_query($sql);
    if ($sql != '') {
        fwrite($fp, $sql . ";\r\n");

    }
    $column_coupon_contract_detail = get_columns_from_table('coupon_contract_detail');
    $data_coupon_contract_details = get_data_from_table_2('coupon_contract_detail', array('contract_id' => $contract_id));
    foreach ($data_coupon_contract_details as &$data_coupon_contract_detail) {
        $order_detail_id = $data_coupon_contract_detail['order_detail_id'];
        $data_coupon_contract_detail['order_detail_id'] = "@order_detail_id" . $mapping_order_detail[$order_detail_id];
        $sql = export_mysql_string_3('coupon_contract_detail', $column_coupon_contract_detail, $data_coupon_contract_detail);
        $sql = replace_query($sql);
        $sql = str_replace('"@order_detail_id' . $mapping_order_detail[$order_detail_id] . '"', '@order_detail_id' . $mapping_order_detail[$order_detail_id], $sql);
        if ($sql != '') {
            fwrite($fp, $sql . ";\r\n");

        }
    }


    $column_coupon_acquisition = get_columns_from_table('coupon_acquisition');
    $data_coupon_acquisitions = get_data_from_table_2('coupon_acquisition', array('contract_id' => $contract_id));
    foreach ($data_coupon_acquisitions as &$data_coupon_acquisition) {
        $order_detail_id = $data_coupon_acquisition['order_detail_id'];
        $data_coupon_acquisition['order_detail_id'] = "@order_detail_id" . $mapping_order_detail[$order_detail_id];
        $sql = export_mysql_string_3('coupon_acquisition', $column_coupon_acquisition, $data_coupon_acquisition);
        $sql = replace_query($sql);
        $sql = str_replace('"@order_detail_id' . $mapping_order_detail[$order_detail_id] . '"', '@order_detail_id' . $mapping_order_detail[$order_detail_id], $sql);
        if ($sql != '') {
            fwrite($fp, $sql . ";\r\n");

        }
    }


    // PROCESS EXPORT contract_server_log
    $column_contract_server_log = get_columns_from_table('contract_server_log');
    $data_contract_server_logs = get_data_from_table_2('contract_server_log', array('contract_id' => $contract_id));
    foreach ($data_contract_server_logs as &$data_contract_server_log) {
        $order_detail_id = $data_contract_server_log['order_detail_id'];
        $data_contract_server_log['order_detail_id'] = "@order_detail_id" . $mapping_order_detail[$order_detail_id];
        $sql = export_mysql_string_3('contract_server_log', $column_contract_server_log, $data_contract_server_log);
        $sql = replace_query($sql);
        $sql = str_replace('"@order_detail_id' . $mapping_order_detail[$order_detail_id] . '"', '@order_detail_id' . $mapping_order_detail[$order_detail_id], $sql);
        if ($sql != '') {
            fwrite($fp, $sql . ";\r\n");

        }
    }

    // PROCESS EXPORT customer_login_id_history

    $data = get_data_from_table('customer_login_id_history', array('customer_id' => $contract_id));
    $array_col = get_columns_from_table('customer_login_id_history');
    $sql = export_mysql_string_2('customer_login_id_history', $array_col, $data);
    $sql = replace_query($sql);
    if ($sql != '') {
        fwrite($fp, $sql . ";\r\n");
    }

    // PROCESS EXPORT amazon_api_log
    $list_order_detail_id = array_keys($mapping_order_detail);
    $column_amazon_api_log = get_columns_from_table('amazon_api_log');
    $data_amazon_api_logs = get_data_from_table_2('amazon_api_log', array('order_detail_id' => $list_order_detail_id));
    foreach ($data_amazon_api_logs as &$data_amazon_api_log) {
        $order_detail_id = $data_amazon_api_log['order_detail_id'];
        $data_amazon_api_log['order_detail_id'] = "@order_detail_id" . $mapping_order_detail[$order_detail_id];
        $sql = export_mysql_string_3('amazon_api_log', $column_amazon_api_log, $data_amazon_api_log);
        $sql = replace_query($sql);
        $sql = str_replace('"@order_detail_id' . $mapping_order_detail[$order_detail_id] . '"', '@order_detail_id' . $mapping_order_detail[$order_detail_id], $sql);
        if ($sql != '') {
            fwrite($fp, $sql . ";\r\n");

        }
    }

    // PROCESS EXPORT lottery
    $column_lottery = get_columns_from_table('lottery');
    $data_lotterys = get_data_from_table_2('lottery', array('id' => $contract_id));
    $sql = export_mysql_string_2('lottery', $column_lottery, $data_lotterys);
    $sql = replace_query($sql);
    if ($sql != '') {
        fwrite($fp, $sql . ";\r\n");

    }
    $column_lottery_campaign = get_columns_from_table('lottery_campaign');
    $data_lottery_campaigns = get_data_from_table_2('lottery_campaign', array('contract_id' => $contract_id));
    foreach ($data_lottery_campaigns as &$data_lottery_campaign) {
        $order_detail_id = $data_lottery_campaign['order_detail_id'];
        $data_lottery_campaign['order_detail_id'] = "@order_detail_id" . $mapping_order_detail[$order_detail_id];
        $sql = export_mysql_string_3('lottery_campaign', $column_lottery_campaign, $data_lottery_campaign);
        $sql = replace_query($sql);
        $sql = str_replace('"@order_detail_id' . $mapping_order_detail[$order_detail_id] . '"', '@order_detail_id' . $mapping_order_detail[$order_detail_id], $sql);
        if ($sql != '') {
            fwrite($fp, $sql . ";\r\n");

        }
    }

    // PROCESS EXPORT impossible_cancel_order_detail
    $column_impossible_cancel_order_detail = get_columns_from_table('impossible_cancel_order_detail');
    $data_impossible_cancel_order_details = get_data_from_table_2('impossible_cancel_order_detail', array('order_detail_id' => $list_order_detail_id));
    foreach ($data_impossible_cancel_order_details as &$data_impossible_cancel_order_detail) {
        $order_detail_id = $data_impossible_cancel_order_detail['order_detail_id'];
        $data_impossible_cancel_order_detail['order_detail_id'] = "@order_detail_id" . $mapping_order_detail[$order_detail_id];
        $sql = export_mysql_string_3('impossible_cancel_order_detail', $column_impossible_cancel_order_detail, $data_impossible_cancel_order_detail);
        $sql = replace_query($sql);
        $sql = str_replace('"@order_detail_id' . $mapping_order_detail[$order_detail_id] . '"', '@order_detail_id' . $mapping_order_detail[$order_detail_id], $sql);
        if ($sql != '') {
            fwrite($fp, $sql . ";\r\n");

        }
    }

    // PROCESS EXPORT receipt_download_log
    $column_receipt_download_log = get_columns_from_table('receipt_download_log');
    $data_receipt_download_logs = get_data_from_table_2('receipt_download_log', array('order_detail_id' => $list_order_detail_id));
    foreach ($data_receipt_download_logs as &$data_receipt_download_log) {
        $order_detail_id = $data_receipt_download_log['order_detail_id'];
        $data_receipt_download_log['order_detail_id'] = "@order_detail_id" . $mapping_order_detail[$order_detail_id];
        $sql = export_mysql_string_3('receipt_download_log', $column_receipt_download_log, $data_receipt_download_log);
        $sql = replace_query($sql);
        $sql = str_replace('"@order_detail_id' . $mapping_order_detail[$order_detail_id] . '"', '@order_detail_id' . $mapping_order_detail[$order_detail_id], $sql);
        if ($sql != '') {
            fwrite($fp, $sql . ";\r\n");

        }
    }

    // PROCESS EXPORT settlement_sbps_detail_log_purchase
    $column_settlement_sbps_detail_log_purchase = get_columns_from_table('settlement_sbps_detail_log_purchase');
    $data_settlement_sbps_detail_log_purchases = get_data_from_table_2('settlement_sbps_detail_log_purchase', array('order_detail_id' => $list_order_detail_id));
    foreach ($data_settlement_sbps_detail_log_purchases as &$data_settlement_sbps_detail_log_purchase) {
        $order_detail_id = $data_settlement_sbps_detail_log_purchase['order_detail_id'];
        $data_settlement_sbps_detail_log_purchase['order_detail_id'] = "@order_detail_id" . $mapping_order_detail[$order_detail_id];
        $sql = export_mysql_string_3('settlement_sbps_detail_log_purchase', $column_settlement_sbps_detail_log_purchase, $data_settlement_sbps_detail_log_purchase);
        $sql = replace_query($sql);
        $sql = str_replace('"@order_detail_id' . $mapping_order_detail[$order_detail_id] . '"', '@order_detail_id' . $mapping_order_detail[$order_detail_id], $sql);
        if ($sql != '') {
            fwrite($fp, $sql . ";\r\n");

        }
    }

    // PROCESS EXPORT settlement_sbps_detail_log_purchase_transaction
    $column_settlement_sbps_detail_log_purchase_transaction = get_columns_from_table('settlement_sbps_detail_log_purchase_transaction');
    $data_settlement_sbps_detail_log_purchase_transactions = get_data_from_table_2('settlement_sbps_detail_log_purchase_transaction', array('order_detail_id' => $list_order_detail_id));
    foreach ($data_settlement_sbps_detail_log_purchase_transactions as &$data_settlement_sbps_detail_log_purchase_transaction) {
        $order_detail_id = $data_settlement_sbps_detail_log_purchase_transaction['order_detail_id'];
        $data_settlement_sbps_detail_log_purchase_transaction['order_detail_id'] = "@order_detail_id" . $mapping_order_detail[$order_detail_id];
        $sql = export_mysql_string_3('settlement_sbps_detail_log_purchase_transaction', $column_settlement_sbps_detail_log_purchase_transaction, $data_settlement_sbps_detail_log_purchase_transaction);
        $sql = replace_query($sql);
        $sql = str_replace('"@order_detail_id' . $mapping_order_detail[$order_detail_id] . '"', '@order_detail_id' . $mapping_order_detail[$order_detail_id], $sql);
        if ($sql != '') {
            fwrite($fp, $sql . ";\r\n");

        }
    }
    $t = 0;
}

function get_data_from_table_2($table, $condition = array(), $remove_id = true, $index = '')
{
    $query = "SELECT * FROM $table";
    if ($condition) {
        $numItems = count($condition);
        $i = 0;
        $query .= " WHERE ";
        foreach ($condition as $key => $value) {
            if (is_array($value)) {
                $query .= $key . ' IN (' . implode(',', $value) . ' )';
            } else {
                $query .= $key . ' = ' . $value;
            }
            if (++$i !== $numItems) {
                $query .= ' AND ';
            }

        }
    }
    $result = mysql_query($query);
    $rs_arr = array();
    if ($result) {
        while ($row = mysql_fetch_assoc($result)) {
            if ($remove_id == true) {
                unset($row['id']);
            }
            if (isset($row['contract_id']) && $row['contract_id'] != 0) {
                $row['contract_id'] = "@contract_id";
            }
            if (isset($row['customer_id']) && $row['customer_id'] != 0) {
                $row['customer_id'] = "@contract_id";
            }
            if ($index !== '') {
                if (isset($row['contract_order_id'])) {
                    $row['contract_order_id'] = "@contract_order_id" . $index;
                }
                if (isset($row['order_detail_id'])) {
                    $row['order_detail_id'] = "@order_detail_id" . $index;
                }
                if (isset($row['order_settlement_id'])) {
                    $row['order_settlement_id'] = "@order_settlement_id" . $index;
                }
                if (isset($row['order_product_id'])) {
                    $row['order_product_id'] = "@order_product_id" . $index;
                }
            }
            $rs_arr[] = $row;
        }
    }
    return $rs_arr;
}

if ($_POST) {
    $evn = 'db14';
    $host = '172.16.100.14';
    $user = 'pwater';
    $pass = 'pwater';
    $db = 'pwater';
    $link = mysql_connect($host, $user, $pass);
    mysql_select_db($db, $link);
    $contract_id = $_POST['contract_id'];
    $fpath = $contract_id . '_' . $evn . '_export.sql';
//Get table customer
    $sql1 = export_mysql_string('customer', array('id' => $contract_id));
    $sql1 = str_replace('"LAST_INSERT_ID()"', 'LAST_INSERT_ID()', $sql1);
    $fp = fopen($fpath, 'w+');
    fwrite($fp, $sql1 . ";\r\n");
    fwrite($fp, 'SET @contract_id = LAST_INSERT_ID();' . "\r\n");

//Get table contract
    $data_contract = get_data_from_table('contract', array('id' => $contract_id));
    $array_col_contract = get_columns_from_table('contract');
    $sql2 = export_mysql_string_2('contract', $array_col_contract, $data_contract);
    $sql2 = str_replace('"@contract_id"', '@contract_id', $sql2);
    fwrite($fp, $sql2 . ";\r\n");
    $table_manual = array('apply_cancel', 'contact_log', 'contract_option_product', 'contract_bank_account', 'contract_card_ivr_log', 'contract_demand_status', 'contract_discount', 'contract_force_cancel_date', 'contract_hash',
        'contract_regist_route', 'contract_ryfety', 'contract_ryfety_complete', 'operation_log', 'settlement_bill_log', 'contract_setting_free', 'opt_login');
    foreach ($table_manual as $table) {
        clone_table_manual($table, $contract_id);
    }
    $table_contract = array('contract_acquisition', 'contract_amazon', 'contract_bill', 'contract_delivery',
        'contract_direct_debit', 'contract_enecom', 'contract_paygent', 'contract_retention', 'contract_sbps', 'contract_service_hydrogen');
    foreach ($table_contract as $table) {
        clone_table_history($table, $contract_id);
    }
    clone_table_history('customer_enecom', $contract_id, false);
    process_export_order_data($contract_id);
    fclose($fp);
    $fopen = fopen($fpath, "rb");
    header("Content-Type:application/octet-stream");
    header("Content-Length:" . filesize($fpath));
    header("Content-Disposition:attachment; filename=" . $fpath);
    $fread = fpassthru($fopen);
    fclose($fopen);
}
?>
<!DOCTYPE html>
<html>
<head>
    <title>Clone Contract Premium Water</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <h3>Clone contract pwater</h3>
    <div class="col-md-9">
        <form class="form-inline" method="post" action="">
            <div class="form-group mx-sm-3 mb-2">
                <label for="contract_id" class="sr-only">Contract ID</label>
                <input type="text" name="contract_id" class="form-control" id="contract_id" placeholder="contract_id">
            </div>
            <button type="submit" class="btn btn-primary mb-2">Clone</button>
        </form>
    </div>
</div>
<script src="http://code.jquery.com/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>
